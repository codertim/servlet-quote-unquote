package timforce;


import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

// import java.io.Serializable;


// public class Quote implements Serializable {
@Entity
@Table(name="quotes")
@NamedQuery(name="findAllTheQuotes", query="from Quote q where q.id > ?")
public class Quote {
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	// @Column(name="saying")
	@Basic
	private String saying;
	// private Author author;

	/*
	@Column(name="author_id")
	private Integer authorId;
	*/

	@ManyToOne
	@JoinColumn(name="author_id")
	private Author author;

	public void setAuthor(Author author) {
		this.author = author;
	}

	public Author getAuthor() {
		return this.author;	
	} 

	/*
	@Transient
	String testDontSave = "";
	@Temporal(TemporalType.TIME)
	Date timeCreated;
	@Temporal(TemporalType.DATE)
	Date dateCreated;
	@Lob
	String description;
	*/

	public Quote(String Saying) {
		this.saying = saying;
	}

	public Quote() {}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getSaying() {
		return this.saying;
	}


	public void setSaying(String saying) {
		this.saying = saying;
	}


	/*
	public Author getAuthor() {
		return this.author;
	}


	public void setAuthor(Author author) {
		this.author = author;
	}
	*/


	/*
	public Integer getAuthorId() {
		return this.authorId;
	}



	public void setAuthorId(Integer authorId) {
		this.authorId = authorId;
	}
	*/



	public String toString() {
		return("id: " + getId());
	}

}


