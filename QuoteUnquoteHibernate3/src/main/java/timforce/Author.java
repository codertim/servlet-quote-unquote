package timforce;


import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table (name="authors")
public class Author {
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	private String name;

	@OneToMany(mappedBy="author")
	/*
	@JoinTable(name="authors_quotes", joinColumns=@JoinColumn(name="author_id"),
			inverseJoinColumns=@JoinColumn(name="quote_id"))
	*/
	private Collection<Quote> quotes = new ArrayList<Quote>();

	private Collection<Quote> getQuotes() {
		return quotes;
	}

	public void setQuotes(Collection<Quote> quotes) {
		this.quotes = quotes;
	}

	public Author(String name) {
		this.name = name;
	}

	public Author() {}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String toString() {
		return("id: " + getId());
	}

}


