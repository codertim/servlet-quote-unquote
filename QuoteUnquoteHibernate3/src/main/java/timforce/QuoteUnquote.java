
package timforce;


import java.io.*;
import java.util.Iterator;
import java.util.List;
import javax.servlet.*;
import javax.servlet.http.*;

import timforce.Quote;

import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.hibernate.HibernateException;
import org.hibernate.MappingException;



public class QuoteUnquote extends HttpServlet {
	Configuration  config         = null;
	SessionFactory sessionFactory = null;
	Session        session        = null;


	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		System.out.println("##### QuoteUnquote#doGet - Starting ...");

		try {
			PrintWriter out = response.getWriter();
			out.println("<h2>QuoteUnquote</h2>");
			out.println("<br /><br />");
			out.println("<br /><br />");
			out.println("<a href='add'>Add quote</a>");
			out.println("<br /><br />");
			out.println("<a href='list'>Show quotes</a>");
			out.println("<br /><br />");
			out.flush();
			out.close();
		} catch(java.io.IOException ioe) {
			System.out.println("##### QuoteUnquote#doGet - Excpetion: " + ioe);
		}


		// testHibernate();
	}



	private void testHibernate() {
		System.out.println("##### testHibernate - Starting ...");

		try {
			config = new Configuration().configure();
			config.addClass(Quote.class);
		} catch(org.hibernate.MappingException me) {
			System.out.println("##### testHibernate - ERROR - MappingException = " + me);
		} catch(Exception e) {
			System.out.println("##### testHibernate - ERROR - Error with mapping to cfg.xml file= " + e);
		}

		try {
			sessionFactory = config.buildSessionFactory();
			session = sessionFactory.openSession();
		} catch(org.hibernate.HibernateException he) {
			System.out.println("##### testHibernate - ERROR - HibernateException = " + he);
		}


		Transaction tx = null;

		try {
			try {
				tx = session.beginTransaction();
				Quote quote = new Quote();
				// quote.setId(new Integer(11));
				quote.setSaying("To be or not to be");
				session.save(quote);
				tx.commit();


				// List quotes = session.find("from Quote quote where quote.id < 5");   hibernate 2
				Query query = session.createQuery("from Quote quote where quote.id < 5");
				List quotes = query.list();
				System.out.println("HQL - List = " + quotes);

				System.out.println("\nIterate over results ...");
				Iterator iter = quotes.iterator();
				while(iter.hasNext()) {
					Quote quoteIter = (Quote) iter.next();
					System.out.println("  quote id = " + quoteIter.getId());
					System.out.println("  quote saying = " + quoteIter.getSaying());
				}
			} catch(Exception e) {
				System.out.println("ERROR - Exception =" + e);
				if(tx != null) {
					tx.rollback();
				}
					throw(e);
			} finally {
				session.close();
			}


			sessionFactory.close();

		} catch(HibernateException he) {
			System.out.println("ERROR - Hibernate Exception =" + he);
		} catch(Exception e) {
				System.out.println("ERROR - Exception =" + e);
		}

	}

}

