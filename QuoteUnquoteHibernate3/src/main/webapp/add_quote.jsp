<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Add New Quote</title>
    <%@include file="includes/header_foundation.jsp" %>
    <style>
      body {
        /* from css3 patterns gallery */
        background-color: #c4041e;
        background-image: linear-gradient(90deg, rgba(255,255,255,.07) 50%, transparent 50%),
        linear-gradient(90deg, rgba(255,255,255,.13) 50%, transparent 50%),
        linear-gradient(90deg, transparent 50%, rgba(255,255,255,.17) 50%),
        linear-gradient(90deg, transparent 50%, rgba(255,255,255,.19) 50%);
        background-size: 13px, 29px, 37px, 53px;
      }

      div#date-time {
        float: right;
        text-align: center;
      }

      /* Begin: Stick Footer Section */
      html, body {
        height: 100%;
      }
      div#main-content {
        height: 400px;
        margin-bottom: -200px;
        min-height: 100%;
      }
      div#footer {
        border: 3px solid #555;
        height: 200px;
      }
      /* End: Stick Footer Section */
    </style>
  </head>
  <body>
    <nav class="top-bar">
      <ul class="title-area">
        <li class="name">
          <h1><a href="/">QuoteUnquote</a></h1>
        </li>
      </ul>
      <section class="top-bar-section">
        <ul class="right">
          <li class="name">
            <h1><a href="#">About</a></h1>
          </li>
          <li class="name">
            <h1><a href="#">Contact</a></h1>
          </li>
        </ul>
      </section>
    </nav>

    <div id="main-content">
      <%@ page import="java.util.Date" %>
      <div class="row">
        <div class="large-6 columns">
          My Add Quote Page!
        </div>

        <div class="large-3 columns">
          Testing Testing
        </div>

        <div class="large-3 columns">
          <a href="#" class="button radius small success">Add</a>
        </div>
      </div>
    </div>

    <div id="footer">
      <div id="date-time" class="panel">
        <h6>Current date/time: <%= new java.util.Date() %></h6>
      </div>
      <div>
           This is the footer
      </div>
    </div>

    <jsp:include page="includes/footer_foundation.jsp" />
  </body>
</html>
