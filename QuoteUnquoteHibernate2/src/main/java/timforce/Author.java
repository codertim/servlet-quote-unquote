package timforce;


import java.io.Serializable;


public class Author implements Serializable {

	private Integer id;
	private String name;

	public Author(String name) {
		this.name = name;
	}

	public Author() {}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String toString() {
		return("id: " + getId());
	}

}


