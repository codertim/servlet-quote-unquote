
package timforce;


import java.io.*;
import java.util.Iterator;
import java.util.List;
import javax.servlet.*;
import javax.servlet.http.*;

import timforce.Quote;

import net.sf.hibernate.*;
import net.sf.hibernate.cfg.Configuration;
import net.sf.hibernate.Criteria;
import net.sf.hibernate.expression.Expression;
import net.sf.hibernate.expression.Order;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.MappingException;



public class ListQuotes extends HttpServlet {
	Configuration  config         = new Configuration();
	SessionFactory sessionFactory = null;
	Session        session        = null;
	PrintWriter    out            = null;


	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		doGet(request, response);
	}



	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		System.out.println("##### ListQuotes#doGet - Starting ...");
		response.setContentType("text/html");

		try {
			out = response.getWriter();
		} catch(java.io.IOException ioe) {
			System.out.println("##### ListQuotes#doGet - Excpetion: " + ioe);
		}

		String actionParam = request.getParameter("action");
		System.out.println("##### ListQuotes#doGet - action parameter = " + actionParam);

		if( (actionParam != null) && ("save".equals(actionParam)) ) {
			//
		} else {
			actionListQuotes(out, request);
		}


		out.flush();
		out.close();
	}



	public void actionListQuotes(PrintWriter out, HttpServletRequest req) {
		System.out.println("##### ListQuotes#actionListQuotes - Starting ...");
		out.println("List Quotes");


		try {
			System.out.println("##### ListQuotes#actionListQuotes - Adding Quote and Author classes to config");
			config.addClass(Quote.class);
			config.addClass(Author.class);
		} catch(net.sf.hibernate.MappingException me) {
			System.out.println("##### ListQuotes#actionListQuotes - Error - " + me);
		}



		try {
			System.out.println("##### ListQuotes#actionListQuotes - Building Session Factory ...");
			sessionFactory = config.buildSessionFactory();
			System.out.println("##### ListQuotes#actionListQuotes - Opening Session Factory ...");
			session = sessionFactory.openSession();
		} catch(net.sf.hibernate.HibernateException he) {
			System.out.println("##### ListQuotes#actionListQuotes - Error - " + he);
		}

		System.out.println("##### ListQuotes#actionListQuotes - Looking up quotes ...");


		Transaction tx = null;
		try {
			try {

				// experiment with named query
				System.out.println("Named Query - Starting ...");
				Query query = session.getNamedQuery("findAllTheQuotes").setString("quoteid", "0");
				System.out.println("Named Query - query = " + query);
				/*  ToDO: pagination
					query.setFirstResult(2);
					query.setMaxResults(5);
				*/

				/*  Named query
				List queryList = query.list();
				Iterator iter = queryList.iterator();
				while(iter.hasNext()) {
					Quote quote = (Quote) iter.next();
					System.out.println("Current Quote from Named Query iterator = " + quote.getSaying());
				}
				outputQuotes(out, queryList);
				*/

				// criteria style search
				System.out.println("Creating criteria");
				Criteria criteria = session.createCriteria(Quote.class);
				criteria.add(Expression.gt("id", 0))
					.add(Expression.le("id", 200))
					.addOrder(Order.desc("id"));
				List criteriaList = criteria.list();
				outputQuotes(out, criteriaList);

				/* HQL style search
				tx = session.beginTransaction();
				List quotes = session.find("from Quote quote where quote.id > 1");
				System.out.println("List = " + quotes);
				outputQuotes(out, quotes);
				tx.commit();
				*/	
			} catch(Exception e) {
				System.out.println("##### ListQuotes#actionListQuotes - Error - " + e);
				if(tx != null) {
					tx.rollback();
				}
				throw(e);
			} finally {
				session.close();
			}

			sessionFactory.close();

		} catch(HibernateException he) {
			System.out.println("##### ListQuotes#actionListQuotes - Hibernate Error - " + he);
		} catch(Exception e) {
			System.out.println("##### ListQuotes#actionListQuotes - Error - " + e);
		}
	
			
	}



	public void outputQuotes(PrintWriter out, List quotes) {
		if(quotes != null) {
			if(quotes.size() == 0) {
				System.out.println("##### ListQuotes#outputQuotes - 0 quotes");
				out.println("<br /><br />No quotes found.");
				return;   // early return
			}


			System.out.println("##### ListQuotes#outputQuotes - showing quotes in html table ...");
			out.println("<table border='0'>");
			Iterator iter = quotes.iterator();

			out.println("<tr><th>ID</th><th>Quote</th><th>Author</th></tr>");

			while(iter.hasNext()) {
				Quote quoteIter = (Quote) iter.next();
				out.println("<tr>");
				out.println("<td>" + quoteIter.getId() + "</td>");
				out.println("<td>" + quoteIter.getSaying() + "</td>");
				Author author = quoteIter.getAuthor();
				out.println("<td>" + author.getName() + "</td>");
				out.println("</tr>");	
			}

			out.println("</table>");
		} else {
				System.out.println("##### ListQuotes#outputQuotes - quotes is NULL - nothing to output");
		}
	}

}


