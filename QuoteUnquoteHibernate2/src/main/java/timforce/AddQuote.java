
package timforce;


import java.io.*;
import java.util.Iterator;
import java.util.List;
import javax.servlet.*;
import javax.servlet.http.*;

import timforce.Quote;

import net.sf.hibernate.*;
import net.sf.hibernate.cfg.Configuration;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.MappingException;



public class AddQuote extends HttpServlet {
	Configuration  config         = new Configuration();
	SessionFactory sessionFactory = null;
	Session        session        = null;
	PrintWriter    out            = null;


	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		doGet(request, response);
	}



	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		System.out.println("##### AddQuote#doGet - Starting ...");

		try {
			out = response.getWriter();
		} catch(java.io.IOException ioe) {
			System.out.println("##### AddQuote#doGet - Excpetion: " + ioe);
		}

		String actionParam = request.getParameter("action");
		System.out.println("##### AddQuote#doGet - action parameter = " + actionParam);

		if( (actionParam != null) && ("save".equals(actionParam)) ) {
			actionSaveQuote(out, request);
		} else {
			actionShowForm(out);
		}


		out.flush();
		out.close();

	}



	public void actionShowForm(PrintWriter out) {
		out.println("<h2>Add a Quote</h2>");
		out.println("<br /><br />");
		// html form
		out.println("<form action='add' method='post'>");
		out.println("Quote/Saying:");
		out.println("<input name='action' type='hidden' value='save' />");
		out.println("<input name='saying' type='text' />");
		out.println("<br /><br />");
		out.println("<input type='submit' value='Save' />");
		out.println("<br /><br />");
		out.println("</form>");
	}



	public void actionSaveQuote(PrintWriter out, HttpServletRequest req) {
		String sayingParam = req.getParameter("saying");

		System.out.println("##### AddQuote#actionSaveQuote - saying parameter = " + sayingParam);
		out.println("Save Quote");


		try {
			config.addClass(Quote.class);
		} catch(net.sf.hibernate.MappingException me) {
			System.out.println("##### AddQuote#actionSaveQuote - Error - " + me);
		}

		try {
			sessionFactory = config.buildSessionFactory();
			session = sessionFactory.openSession();
		} catch(net.sf.hibernate.HibernateException he) {
			System.out.println("##### AddQuote#actionSaveQuote - Error - " + he);
		}


		Transaction tx = null;
		try {
			try {
				tx = session.beginTransaction();
				Quote quote = new Quote();
				quote.setSaying(sayingParam);
				session.save(quote);
				tx.commit();
			} catch(Exception e) {
				System.out.println("##### AddQuote#actionSaveQuote - Error - " + e);
				if(tx != null) {
					tx.rollback();
				}
				throw(e);
			} finally {
				session.close();
			}

			sessionFactory.close();

		} catch(HibernateException he) {
			System.out.println("##### AddQuote#actionSaveQuote - Hibernate Error - " + he);
		} catch(Exception e) {
			System.out.println("##### AddQuote#actionSaveQuote - Error - " + e);
		}
	
			
	}

}


