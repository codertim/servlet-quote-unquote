package timforce;


import java.io.Serializable;


public class Quote implements Serializable {

	private Integer id;
	private String saying;
	private Author author;

	public Quote(String Saying) {
		this.saying = saying;
	}

	public Quote() {}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getSaying() {
		return this.saying;
	}


	public void setSaying(String saying) {
		this.saying = saying;
	}


	public Author getAuthor() {
		return this.author;
	}


	public void setAuthor(Author author) {
		this.author = author;
	}


	public String gtoString() {
		return("id: " + getId());
	}

}


